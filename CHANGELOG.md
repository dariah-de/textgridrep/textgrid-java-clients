# [5.2.0](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//compare/v5.1.1...v5.2.0) (2024-11-21)


### Bug Fixes

* add auth to tgcrud reindex metadata operation ([00a8632](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/00a86322158acc05e925eabc992850f5848af28e))
* add reindex operation ([0d34248](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/0d342486050da294a9207696024bb72743d546dd))
* add reindex secret ([af0f216](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/af0f216fc4a52736f1c1ab8c24ce8cfa485b65b4))
* add reindex secret to CrudOperation ([bee48ea](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/bee48ea5f3ef2e9aa711d7981f5ed7d14500e0a5))
* using reindexSecret in ReindexMetadataOperation only, as projectId in create ([5c110db](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/5c110dbeb6bb451c8c0197da52646d5213b96122))


### Features

* add new tgcrud method to client api (copy) ([ead36b7](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/ead36b7b09796164ae81c83864ec586781f8172e))

## [5.1.1](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//compare/v5.1.0...v5.1.1) (2024-09-19)


### Bug Fixes

* readmetadata is reading from reader now, instead of string ([86c6ad3](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/86c6ad3ee8b215d0f238877d49d789a30d53894d))
* use httpUrlConnection if underlaying restclientbuilder implementation is cxf. ([1b41bf5](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/1b41bf54fe834b6a311cb6c40ed389884454b340))

# [5.1.0](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//compare/v5.0.0...v5.1.0) (2024-09-12)


### Bug Fixes

* add  consistency check to crud client ([9278049](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/92780499f512b9b0ba607007cf2b4fbf433c6fd7))
* add check consistency method ([c3c24f8](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/c3c24f8f419f1bf5cb66b1fc5a991cd7e88c6c05))
* change response of check consistency method ([8fb9dd1](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/8fb9dd153afceae5e677c5ec722a3ad2b3a7e525))


### Features

* add ProjectQuery (PortalHelper) to tgsearch client ([24b7a67](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/24b7a67b0adfb4fee493ed52f97e7cc47bf33da4))
* methods for toplevel listing and projectconfig retrieval to portalhelper API ([e368cbe](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/e368cbe529e7e0b697024e109520bc542882c5f4))

# [5.0.0](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//compare/v4.5.0...v5.0.0) (2024-05-23)


### Bug Fixes

* increase versions ([398d96d](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/398d96dc54fe409f1039f6c860a3c28bec2ca712))
* remove formatting! ... ([70e5023](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/70e5023e77f15a0a830413adce61229f88410269))
* typos in rest path for tgsearch ([004f874](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/004f87448962bd957e57801dce71225c8aeeec3c))


### Code Refactoring

* move to jee9, java17, eclipse microprofile rest client ([2b8301b](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/2b8301b4e8b9b23e1fd85d088f9904aa1662cfe9))


### BREAKING CHANGES

* needs java > 17 and eclipse microprofile rest client implementation now.

# [4.5.0](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//compare/v4.4.3...v4.5.0) (2023-06-21)


### Features

* **SearchClient:** facet query allows setting sid now ([1c35016](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/1c35016f0f105ecab479ed0082b78e29c3f8e5bd))

## [4.4.3](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//compare/v4.4.2...v4.4.3) (2022-11-17)


### Bug Fixes

* add sbom handling ([6bcea12](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/6bcea1278c15125b6fbf91b44d17806387f236cc))
* remove sbom again (done by parent pom!) ([135ae24](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients//commit/135ae248232c263060360017003a88a3d297ee2e))
