/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright Göttingen State and University Library (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.api;

import java.net.URI;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.activation.DataHandler;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * <p>
 * This API provides REST access to TG-crud methods.
 * 
 * NOTE: Copied from info.textgrid.middleware.tgcrud-api due to dependency handling!
 * </p>
 **/

public interface TGCrudServiceREST {

  // **
  // STATIC FINALS
  // **

  /** Parameter name for RBAC session ID */
  public static final String PARAM_SESSION_ID = "sessionId";
  /** Parameter name for re-index secret */
  public static final String PARAM_REINDEX_SECRET = "reindexSecret";
  /** Parameter name for the log parameter */
  public static final String PARAM_LOG_PARAMETER = "logParameter";
  /** Parameter name for the TextGrid URI */
  public static final String PARAM_URI = "uri";
  /** Parameter name for creating a new revision */
  public static final String PARAM_CREATE_REVISION = "createRevision";
  /** Parameter name for the TextGrid project ID */
  public static final String PARAM_PROJECT_ID = "projectId";
  /** Parameter name for the amount of URIs to create */
  public static final String PARAM_HOW_MANY = "howMany";
  /** Parameter name for the TextGrid object data */
  public static final String PARAM_TGOBJECT_DATA = "tgObjectData";
  /** Parameter name for the TextGrid object metadata */
  public static final String PARAM_TGOBJECT_METADATA = "tgObjectMetadata";

  // **
  // METHOD DECLARATIONS
  // **

  /**
   * <p>
   * The RESTful method API for the TG-crud#CREATE method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param baseUri The base URI
   * @param createRevision To create a new revision or not to create a new revision
   * @param projectId The TextGrid project ID
   * @param tgObjectMetadata The TextGrid object metadata
   * @param tgObjectData The TextGrid object data
   * @return Multipart TextGrid object containing data and metadata
   */
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/create")
  public Response createREST(@QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @QueryParam(PARAM_URI) URI baseUri,
      @QueryParam(PARAM_CREATE_REVISION) Boolean createRevision,
      @QueryParam(PARAM_PROJECT_ID) String projectId,
      @Multipart(value = PARAM_TGOBJECT_METADATA,
          type = MediaType.TEXT_XML) MetadataContainerType tgObjectMetadata,
      @Multipart(value = PARAM_TGOBJECT_DATA,
          type = MediaType.APPLICATION_OCTET_STREAM) DataHandler tgObjectData);

  /**
   * <p>
   * The RESTful method API for the TG-crud#CREATEMETADATA method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param baseUri The base URI
   * @param projectId The TextGrid project ID
   * @param externalReference The external reference for metadata-only objects
   * @param tgObjectMetadata The TextGrid object metadata
   * @return Multipart TextGrid object containing data and metadata
   */
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/createMetadata")
  public Response createMetadataREST(String sessionId, String logParameter,
      URI baseUri, String projectId, URI externalReference,
      MetadataContainerType tgObjectMetadata);

  /**
   * <p>
   * The RESTful method API for the TG-crud#DELETE method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param baseUri The base URI
   */
  @GET
  @Path("/{uri}/delete")
  public void deleteREST(@QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @PathParam(PARAM_URI) URI baseUri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#GETURI method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param howMany The amount of URIs to create
   * @return List of TextGrid URIs
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getUri")
  public Response getUriREST(@QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @QueryParam(PARAM_HOW_MANY) int howMany);

  /**
   * <p>
   * The RESTful method API for the TG-crud#GETVERSION method.
   * </p>
   *
   * @return TG-crud Version string
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/version")
  public String getVersionREST();

  /**
   * <p>
   * The RESTful method API for the TG-crud#READ method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param baseUri The base URI
   * @return TextGrid data object
   */
  @GET
  @Path("/{uri: .+}/data")
  public Response readREST(@QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @PathParam(PARAM_URI) URI baseUri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#READMETADATA method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param baseUri The base URI
   * @return TextGrid metadata object
   */
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri: .+}/metadata")
  public Response readMetadataREST(
      @QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @PathParam(PARAM_URI) URI baseUri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#READTECHMD method.
   * </p>
   *
   * @param uri The URI to read technical metadata for
   * @return HTTP response with technical metadata FITS XML
   */
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri: .+}/tech")
  public Response readTechmdREST(final @PathParam(PARAM_URI) URI uri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#UPDATE method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param uri The URI of the object to update
   * @param tgObjectMetadata The TextGrid object metadata
   * @param tgObjectData The TextGrid object data
   * @return TextGrid metadata object
   */
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri}/update")
  public Response updateREST(@QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @PathParam(PARAM_URI) URI uri,
      @Multipart(value = PARAM_TGOBJECT_METADATA,
          type = MediaType.TEXT_XML) MetadataContainerType tgObjectMetadata,
      @Multipart(value = PARAM_TGOBJECT_DATA,
          type = MediaType.APPLICATION_OCTET_STREAM) DataHandler tgObjectData);

  /**
   * <p>
   * The RESTful method API for the TG-crud#UPDATEMETADATA method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param uri The URI of the object to update metadata
   * @param tgObjectMetadata The TextGrid object metadata
   * @return TextGrid metadata object
   */
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri}/updateMetadata")
  public Response updateMetadataREST(
      @QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @PathParam(PARAM_URI) URI uri,
      @Multipart(value = PARAM_TGOBJECT_METADATA,
          type = MediaType.TEXT_XML) MetadataContainerType tgObjectMetadata);

  /**
   * <p>
   * The RESTful method API for the TG-crud#LOCK method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param uri The URI of the object to lock
   * @return TRUE if locking succeeded, FALSE if not
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{uri}/lock")
  public boolean lockREST(@QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @PathParam(PARAM_URI) URI uri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#UNLOCK method.
   * </p>
   *
   * @param sessionId The RBAC session ID
   * @param logParameter The log parameter
   * @param uri The URI of the object to unlock
   * @return TRUE if unlocking succeeded, FALSE if not.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{uri}/unlock")
  public boolean unlockREST(@QueryParam(PARAM_SESSION_ID) String sessionId,
      @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      @PathParam(PARAM_URI) URI uri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#NUMBER4NOID method (REST only).
   * </p>
   *
   * @param uri The URI of the object to get the number for the NOID
   * @return The decimal number for a NOID TextGrid URI
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{uri}/n4n")
  public String number4noidREST(@PathParam(PARAM_URI) URI uri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#RECACHE method (REST only).
   *
   * Checks and re-caches image file, if necessary. Sends an UPDATE message via TG-crud to messaging
   * service (Wildfly).
   * </p>
   *
   * @param uri The URI of the object to send an update message for
   * @return HTTP response of the update message sending call.
   */
  @GET
  @Path("/{uri}/recache")
  public Response recacheREST(@PathParam(PARAM_URI) URI uri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#CONSISTENCY method (REST only).
   * 
   * Checks consistency of filesize and checksum in (i) storage, (ii) Handle metadata, and (iii)
   * TextGrid metadata.
   * </p>
   * 
   * @param uri The URI of the object to check
   * @return HTTP response of the consistency check: NO CONTENT if successful, OK and JSON response
   *         body otherwise.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{uri}/consistency")
  public Response checkConsistencyREST(@PathParam(PARAM_URI) URI uri);

  /**
   * <p>
   * The RESTful method API for the TG-crud#REINDEX method (REST only).
   * 
   * Reads metadata from the storage and and puts them in ElasticSearch as a metadata update, aka
   * re-index.
   * </p>
   * 
   * @param auth The secret for using this re-index metadata method.
   * @param uri The URI of the object to re-index.
   * @return HTTP response of the re-index: NO CONTENT if successful.
   */
  @GET
  @Path("/{uri}/reindexMetadata")
  public Response reindexMetadataREST(@QueryParam(PARAM_REINDEX_SECRET) String auth,
      @PathParam(PARAM_URI) URI uri);

}
