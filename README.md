# The TextGrid Java Clients

The TextGrid Java Clients provide an easy API for the TextGrid repository services.

## Documentation

Check <https://textgridlab.org/doc/services/submodules/textgrid-java-clients/docs/index.html> for stable and <https://dev.textgridlab.org/doc/services/submodules/textgrid-java-clients/docs/index.html> for the development version of the documentation.

The source for the generated documentation is in the [docs](./docs/) folder.

## Releasing a new version

For releasing a new version of the TextGrid Java Clients, please have a look at the [DARIAH-DE Release Management Page](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management) or see the [Gitlab CI file](.gitlab-ci.yml).
