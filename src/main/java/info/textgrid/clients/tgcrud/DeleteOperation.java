package info.textgrid.clients.tgcrud;

import java.net.URI;
import java.net.URISyntaxException;

import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

public class DeleteOperation extends CrudOperation {

  public DeleteOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  public void execute() throws CrudClientException {

    try {
      getTgcrud().deleteREST(getSid(), "", new URI(getTextgridUri()));
    } catch (URISyntaxException e) {
      throw new CrudClientException(e);
    }

  }

  /**
   * Set TextGrid URI for Object to operate on
   *
   * @param textgridUri
   * @return
   */
  public DeleteOperation setTextgridUri(String textgridUri) {
    super.setTextgridUri(textgridUri);
    return this;
  }

  /**
   * Set TextGrid SessionID (sid)
   *
   * @param sid
   * @return
   */
  public DeleteOperation setSid(String sid) {
    super.setSid(sid);
    return this;
  }

}
