package info.textgrid.clients.tgauth;

public class AuthClientException extends Exception {

	private static final long serialVersionUID = 5303767992513491073L;

	public AuthClientException() {
		// TODO Auto-generated constructor stub
	}

	public AuthClientException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AuthClientException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public AuthClientException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AuthClientException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
