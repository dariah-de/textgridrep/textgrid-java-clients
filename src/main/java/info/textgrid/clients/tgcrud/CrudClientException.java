package info.textgrid.clients.tgcrud;

public class CrudClientException extends Exception {

	private static final long serialVersionUID = 3917062110932297964L;

	public CrudClientException() {
		// TODO Auto-generated constructor stub
	}

	public CrudClientException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CrudClientException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CrudClientException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CrudClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
