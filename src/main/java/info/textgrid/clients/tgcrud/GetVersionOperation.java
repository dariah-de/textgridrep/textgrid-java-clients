package info.textgrid.clients.tgcrud;

import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

public class GetVersionOperation extends CrudOperation {

  public GetVersionOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  /**
   * Get the TextGrid CRUD version
   *
   * @see TextGridObject
   * @return the TextgridObject
   * @throws CrudClientException
   */
  public String execute() throws CrudClientException {
    return getTgcrud().getVersionREST();
  }

}
