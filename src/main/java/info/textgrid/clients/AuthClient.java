package info.textgrid.clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import info.textgrid.clients.tgauth.AuthClientException;
import info.textgrid.middleware.tgauth.tgauthclient.TGAuthClientCrudUtilities;
import info.textgrid.middleware.tgauth.tgauthclient.TGAuthClientUtilities;
import info.textgrid.namespaces.middleware.tgauth.AddMemberRequest;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectResponse;
import info.textgrid.namespaces.middleware.tgauth.DeleteProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.GetAllProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetMyUserAttributesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetMyUserAttributesResponse;
import info.textgrid.namespaces.middleware.tgauth.GetNamesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetNamesResponse;
import info.textgrid.namespaces.middleware.tgauth.GetProjectDescriptionRequest;
import info.textgrid.namespaces.middleware.tgauth.NotEmptyFault;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.ProjectInfo;
import info.textgrid.namespaces.middleware.tgauth.RbacFault;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.UserAttribute;
import info.textgrid.namespaces.middleware.tgauth.UserDetail;
import info.textgrid.namespaces.middleware.tgauth_crud.GetEPPNRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;

/**
 * Client for TG-Auth
 *
 * @see <a href=
 *      "https://textgridlab.org/doc/services/submodules/tg-auth/docs/index.html">TG-Auth
 *      documentation</a>
 * @author Ubbo Veentjer
 */
public class AuthClient {

  /**
   * AUTHZ Instance on textgrid production system
   */
  public static String DEFAULT_AUTHZ_INSTANCE = "textgrid-esx2.gwdg.de";

  /**
   * URL for script to login and retrieve SID on textgrid production system
   * (textgridlab.org)
   */
  public static String DEFAULT_WEBAUTH_URL = "https://textgridlab.org/1.0/WebAuthN/TextGrid-WebAuth.php";

  /**
   * URL for tgextra textgrid production system (textgridlab.org)
   */
  public static String DEFAULT_TGEXTRA_URL = "https://textgridlab.org/1.0/tgauth/tgextra.php";
  public static String DEFAULT_TGEXTRA_CRUD_URL = "https://textgridlab.org/1.0/tgauth/tgextra-crud.php";

  private static final String TG_STANDARD_ROLE_MANAGER = "Projektleiter";
  private static final String TG_STANDARD_ROLE_ADMINISTRATOR = "Administrator";
  private static final String TG_STANDARD_ROLE_EDITOR = "Bearbeiter";
  private static final String TG_STANDARD_ROLE_OBSERVER = "Beobachter";

  private String authzInstance;
  // TODO: does this make sense, could be dangerous in multi user env?
  private String sid;
  private URL webauthUrl;
  private PortTgextra tgextraClient;
  private PortTgextraCrud tgextraClientCrud;

  /**
   * @throws AuthClientException
   */
  public AuthClient() throws AuthClientException {
    this.authzInstance = DEFAULT_AUTHZ_INSTANCE;

    try {
      this.webauthUrl = new URL(DEFAULT_WEBAUTH_URL);
    } catch (MalformedURLException e) {
      throw new AuthClientException(e);
    }

    // tgextra and tgextra-crud client setup
    try {
      this.tgextraClient = TGAuthClientUtilities
          .getTgextra(new URL(DEFAULT_TGEXTRA_URL));
      this.tgextraClientCrud = TGAuthClientCrudUtilities
          .getTgextraCrud(new URL(DEFAULT_TGEXTRA_CRUD_URL));
    } catch (MalformedURLException e) {
      throw new AuthClientException(e.getMessage());
    }
  }

  /**
   * List all projects visible to the owner of a TextGrid-SessionID (sid)
   *
   * @param sid
   * @return
   */
  public List<String> getProjectsForSID(String sid) {

    TgAssignedProjectsRequest tgAssignedProjectsInput = new TgAssignedProjectsRequest();
    tgAssignedProjectsInput.setAuth(sid);
    return this.tgextraClient.tgAssignedProjects(tgAssignedProjectsInput)
        .getRole();
  }

  /**
   * No SID needed? good / bad?
   *
   * @return
   */
  public List<ProjectInfo> getAllProjects() {

    GetAllProjectsRequest getAllProjectsRequest = new GetAllProjectsRequest();
    // getAllProjectsRequest.setAuth(sid);
    return this.tgextraClient.getAllProjects(getAllProjectsRequest)
        .getProject();
  }

  /**
   * no SID needed? good/bad?
   *
   * @param projectId
   * @return
   */
  public ProjectInfo getProjectInfo(String projectId) {

    GetProjectDescriptionRequest getProjectDescriptionRequest = new GetProjectDescriptionRequest();
    // getProjectDescriptionRequest.setAuth(sid);
    getProjectDescriptionRequest.setProject(projectId);
    return this.tgextraClient
        .getProjectDescription(getProjectDescriptionRequest)
        .getProject();
  }

  /**
   * @param sid
   * @param name
   * @param description
   * @param allRights
   * @return
   * @throws AuthClientException
   */
  public String createProject(String sid, String name, String description,
      boolean allRights) throws AuthClientException {

    CreateProjectRequest createProjectRequest = new CreateProjectRequest();
    createProjectRequest.setAuth(sid);
    createProjectRequest.setName(name);
    createProjectRequest.setDescription(description);
    CreateProjectResponse res = this.tgextraClient
        .createProject(createProjectRequest);

    if (allRights) {
      addEditorToProject(res.getProjectId(), getEppnForSid(sid), sid);
      addAdminToProject(res.getProjectId(), getEppnForSid(sid), sid);
    }

    return res.getProjectId();
  }

  /**
   * delete project
   *
   * @param projectId
   *                  project id
   * @param sid
   *                  sessionid
   * @throws NotEmptyFault
   *                             if project not empty
   * @throws AuthClientException
   */
  public void deleteProject(String projectId, String sid)
      throws NotEmptyFault, AuthClientException {

    DeleteProjectRequest deleteProjectInput = new DeleteProjectRequest();
    deleteProjectInput.setAuth(sid);
    deleteProjectInput.setProject(projectId);

    try {
      this.tgextraClient.deleteProject(deleteProjectInput);
    } catch (AuthenticationFault e) {
      throw new AuthClientException(e);
    }
  }

  /**
   * editor may edit files (TODO: link to doku of rights)
   *
   * @param projectId
   * @param eppn
   * @param sid
   * @throws AuthClientException
   */
  public void addEditorToProject(String projectId, String eppn, String sid)
      throws AuthClientException {
    addRoleToProject(projectId, eppn, TG_STANDARD_ROLE_EDITOR, sid);
  }

  /**
   * Admin has "authority to delete" (TODO: link to doku of rights)
   *
   * @param projectId
   * @param eppn
   * @param sid
   * @throws AuthClientException
   */
  public void addAdminToProject(String projectId, String eppn, String sid)
      throws AuthClientException {
    addRoleToProject(projectId, eppn, TG_STANDARD_ROLE_ADMINISTRATOR, sid);
  }

  /**
   * Observer
   *
   * @param projectId
   * @param eppn
   * @param sid
   * @throws AuthClientException
   */
  public void addObserverToProject(String projectId, String eppn, String sid)
      throws AuthClientException {
    addRoleToProject(projectId, eppn, TG_STANDARD_ROLE_OBSERVER, sid);
  }

  /**
   * Project manager
   *
   * @param projectId
   * @param eppn
   * @param sid
   * @throws AuthClientException
   */
  public void addManagerToProject(String projectId, String eppn, String sid)
      throws AuthClientException {
    addRoleToProject(projectId, eppn, TG_STANDARD_ROLE_MANAGER, sid);
  }

  /**
   * @param projectId
   * @param eppn
   * @param role
   * @param sid
   * @throws AuthClientException
   */
  private void addRoleToProject(String projectId, String eppn, String role,
      String sid) throws AuthClientException {

    AddMemberRequest addMemberRequest = new AddMemberRequest();
    addMemberRequest.setAuth(sid);
    addMemberRequest.setUsername(eppn);

    final String FQRoleName = role + "," + projectId + ","
        + "Projekt-Teilnehmer";

    addMemberRequest.setRole(FQRoleName);

    try {
      this.tgextraClient.addMember(addMemberRequest);
    } catch (RbacFault e) {
      throw new AuthClientException(e);
    }
  }

  /**
   * Guess no one needs - Remove ?
   *
   * @param sid
   * @return
   * @throws AuthClientException
   */
  private List<UserAttribute> getUserAttributes(String sid)
      throws AuthClientException {

    GetMyUserAttributesRequest arg0 = new GetMyUserAttributesRequest();
    arg0.setAuth(sid);
    GetMyUserAttributesResponse res;
    try {
      res = this.tgextraClient.getMyUserAttributes(arg0);
    } catch (AuthenticationFault e) {
      throw new AuthClientException(e);
    }

    return res.getAttribute();

  }

  /**
   * @param sid
   * @return
   * @throws AuthClientException
   */
  public String getEppnForSid(String sid) throws AuthClientException {

    GetEPPNRequest getEPPNInput = new GetEPPNRequest();
    getEPPNInput.setAuth(sid);
    try {
      return this.tgextraClientCrud.getEPPN(getEPPNInput).getEppn();
    } catch (info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      throw new AuthClientException(e);
    }
  }

  /**
   * @param user
   * @param pass
   * @return
   */
  public String getSid(String user, String pass) {

    String sid = "";

    try {

      Form form = new Form();
      form.param("loginname", user);
      form.param("password", pass);
      form.param("authZinstance", this.authzInstance);

      System.out.println("get sid for " + user + " / webauthurl: "
          + this.webauthUrl.toString());

      Client client = ClientBuilder.newClient();

      WebTarget target = client
          .target(this.webauthUrl.getProtocol() + "://"
              + this.webauthUrl.getHost())
          .path(this.webauthUrl.getPath());

      Response res = target.request().post(Entity.entity(form,
          MediaType.APPLICATION_FORM_URLENCODED_TYPE));

      // res.g

      // System.out.println();
      // IOUtils.copy((InputStream) res.getEntity(), System.out);

      BufferedReader reader = new BufferedReader(
          new InputStreamReader((InputStream) res.getEntity()));

      String line;
      while ((line = reader.readLine()) != null) {
        if (line.startsWith("<meta name=\"rbac_sessionid")) {
          int cpos = line.indexOf("content=");
          int bpos = line.indexOf("\"", cpos) + 1;
          int epos = line.indexOf("\"", bpos);
          sid = line.substring(bpos, epos);
          break;
        }
      }

    } catch (IOException e) {
      System.out.println("error getting sid ");
    }
    this.sid = sid;
    return sid;
  }

  /**
   * @return
   */
  public String getSid() {
    return this.sid;
  }

  /**
   * @param sid
   * @return
   */
  public AuthClient setSid(String sid) {
    this.sid = sid;
    return this;
  }

}
