###
# PREPARATIONS
###

# Some variables
# TODO: Apply also as a template!
variables:
  # Maven Settings
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
  # Maven deployment settings for Nexus, please use env vars in Settings -> CI/CD -> Variables.
  MAVEN_SETTINGS_PATH: ".m2/settings.xml"
  MAVEN_DEPLOY_OPTS: "--settings=$MAVEN_SETTINGS_PATH"
  MAVEN_IMAGE: "maven:3.8.7-eclipse-temurin-17"
  # Semantic versioning commit and push vars (the TOKEN name and user)
  GIT_AUTHOR_EMAIL: ${GL_USER}@noreply.gitlab.gwdg.de
  GIT_AUTHOR_NAME: ${GL_USER}
  GIT_COMMITTER_EMAIL: ${GL_USER}@noreply.gitlab.gwdg.de
  GIT_COMMITTER_NAME: ${GL_USER}
 
# Include Java settings from Gitlab templates repo.
include:
  - project: 'dariah-de/gitlab-templates'
    ref: 'main'
    file:
      - '/templates/.java.gitlab-ci.yml'
      - '/templates/SBOM-Upload.gitlab-ci.yml'

# Stages...
stages:
  - prepare
  - test
  - build
  - deploy

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  key: $CI_JOB_NAME
  paths:
    - .m2/repository

###
# JOBS
###

# Update POM and CHANGELOG RELEASE files, TAG a new RELEASE version!
prepare-release:
  stage: prepare
  rules:
  # Only run if branch "main" AND commit title IS NOT "1.2.3" (main commit with tag) AND DOES NOT START WITH "Prepare next development iteration" (main commit with new version)
    - if: $CI_COMMIT_REF_NAME == "main" && $CI_COMMIT_TITLE !~ /^[\d\.]+$/ && $CI_COMMIT_TITLE !~ /^Prepare next development iteration/
  image: $MAVEN_IMAGE
  extends:
    - .prepare-semantic-release

# Get the project version from main POM file and store it to artifact.
get-pom-version:
  stage: prepare
  only:
    - develop
    - tags
  extends:
    - .get-pom-version-to-env

# Build, validate, and package all the feature branches
validate-java:
  image: $MAVEN_IMAGE
  stage: test
  except:
    - main
    - develop
    - tags
  script:
    - mvn $MAVEN_OPTS $MAVEN_CLI_OPTS $MAVEN_DEPLOY_OPTS -U clean validate package
  artifacts:
    reports:
      # Declare the JUnit reports (recursive pattern for multi-module projects)
      junit:
        - "**/target/*-reports/TEST-*.xml"

# Build and deploy all JARs to GWDG Nexus (develop -> SNAPSHOT and tags -> RELEASE)!
# NOTE mvn deploy triggers BOM creation!
build-and-deploy-jars:
  image: $MAVEN_IMAGE
  stage: build
  only:
    - develop
    - tags
  script:
    - mvn $MAVEN_OPTS $MAVEN_CLI_OPTS $MAVEN_DEPLOY_OPTS -U clean validate deploy -Psbom
    - cp target/bom.json .
  artifacts:
    paths:
      - ./bom.json
    reports:
      # Declare the JUnit reports (recursive pattern for multi-module projects)
      junit:
        - "**/target/*-reports/TEST-*.xml"
