package info.textgrid.clients.tgsearch;

import info.textgrid.middleware.tgsearch.api.Navigation;
import info.textgrid.namespaces.middleware.tgsearch.Response;

public class NavigationQuery {

  private String sid;
  private Navigation nav;
  private boolean searchSandbox = false;

  public NavigationQuery(Navigation nav) {
    this.nav = nav;
  }

  public String getSid() {
    return sid;
  }

  public NavigationQuery setSid(String sid) {
    this.sid = sid;
    return this;
  }

  public boolean getSearchSandbox() {
    return searchSandbox;
  }

  public NavigationQuery setSearchSandbox(boolean searchSandbox) {
    this.searchSandbox = searchSandbox;
    return this;
  }

  /**
   * List contents of a textgridproject
   *
   * @param id
   *           id of a textgrid project
   * @return
   *         tgsearch response object containing results
   */
  public Response listProject(String id) {
    return nav.listProject(id, sid, "");
  }

  /**
   * List toplevel aggregations from public textgrid repository
   *
   * @return
   *         tgsearch response
   */
  public Response listToplevelAggregations() {
    return nav.listToplevelAggregations(searchSandbox);
  }

  /**
   * List contents of an aggregation
   *
   * @param uri
   *            uri of an textgrid aggregation
   * @return
   *         tgsearch response object containing results
   */
  public Response listAggregation(String uri) {
    return nav.listAggregation(uri, sid, "");
  }

}
