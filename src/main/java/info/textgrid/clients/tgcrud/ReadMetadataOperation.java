package info.textgrid.clients.tgcrud;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;
import jakarta.xml.bind.JAXB;

/**
 *
 */
public class ReadMetadataOperation extends CrudOperation {

  /**
   * @param tgcrud
   */
  public ReadMetadataOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  /**
   * Get the TextGrid Metadata object with TextGridUri
   *
   * @see TextGridObject
   * @return the TextgridObject
   * @throws CrudClientException
   */
  public MetadataContainerType execute() throws CrudClientException {
    try {

      MetadataContainerType result;

      // Get string result from TG-crud.
      String res = getTgcrud().readMetadataREST(getSid(), "", new URI(getTextgridUri()))
          .readEntity(String.class);

      // Create reader to let JAXB read from, JAXB does not read from strings, it instead is trying
      // to read from a file with the string as a filename!
      try (Reader reader = new StringReader(res)) {
        result = JAXB.unmarshal(reader, MetadataContainerType.class);
      }

      return result;

    } catch (URISyntaxException | IOException e) {
      throw new CrudClientException(e);
    }
  }

  /**
   * Set TextGrid URI for Object to operate on
   *
   * @param textgridUri
   * @return
   */
  @Override
  public ReadMetadataOperation setTextgridUri(String textgridUri) {
    super.setTextgridUri(textgridUri);
    return this;
  }

  /**
   * Set a TextGrid SessionID (sid), only needed for reading on private data
   *
   * @param sid
   * @return
   */
  @Override
  public ReadMetadataOperation setSid(String sid) {
    super.setSid(sid);
    return this;
  }

}
