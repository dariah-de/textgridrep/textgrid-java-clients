package info.textgrid.clients.tgcrud;

import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;

import java.net.URI;
import java.net.URISyntaxException;

import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

public class CreateOperation extends CrudOperation {

  private TextGridObject textgridObject;
  private String projectId;

  public CreateOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  /**
   * Create the Object
   * 
   * @return TextGridURI of created Object
   * @throws CrudClientException
   */
  public String execute() throws CrudClientException {

    DataSource dataSource = new InputStreamDataSource(this.textgridObject.getData(), "");
    DataHandler data = new DataHandler(dataSource);


    try {
      getTgcrud().createREST(getSid(), "", new URI(null), false,
          this.projectId, this.textgridObject.getMetadatada(), data);
    } catch (URISyntaxException e) {
      throw new CrudClientException(e);
    }

    return this.textgridObject.getMetadatada().getObject().getGeneric().getGenerated()
        .getTextgridUri().getValue();

  }

  /**
   * Set TextGrid SessionID (sid)
   * 
   * @param sid
   * @return
   */
  public CreateOperation setSid(String sid) {
    super.setSid(sid);
    return this;
  }

  /**
   * TextGrid-Object (Data and Metadata) to be stored in TextGridRep
   * 
   * @see TextGridObject
   * @param textgridObject
   * @return
   */
  public CreateOperation setObject(TextGridObject textgridObject) {
    this.textgridObject = textgridObject;
    return this;
  }

  /**
   * Set URI of project, where the object should be created in
   * 
   * @param projectId
   * @return
   */
  public CreateOperation setProjectId(String projectId) {
    this.projectId = projectId;
    return this;
  }

}
