package info.textgrid.clients;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.xml.bind.JAXB;

/**
 * This tests assume that the TG-crud is available (and some data is available
 * for reading via TextGrid URI)
 *
 * @author Stefan E. Funk, SUB Göttingen
 */
public class CrudClientIntegrationTest {

  static final String TGCRUD_PROD_PUBLIC_URL = CrudClient.DEFAULT_ENDPOINT;
  //static final String TGCRUD_PROD_PUBLIC_URL = CrudClient.DEFAULT_DEV_ENDPOINT;

  static CrudClient crudClient;

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUp() throws Exception {
    crudClient = new CrudClient(TGCRUD_PROD_PUBLIC_URL);
  }

  /**
   * Testing #GETVERSION
   *
   * @throws CrudClientException
   */
  @Test
  public void getVersion() throws CrudClientException {
    String version = crudClient.getVersion().execute();
    System.out.println(version);
    assertNotNull("TG-crud version must not be null!", version);
    assertNotEquals("TG-crud version must not be empty!", version, "");
  }

  /**
   * Testing #READMETADATA
   *
   * @throws CrudClientException
   */
  @Test
  public void getAliceMD() throws CrudClientException {
    MetadataContainerType res =
        crudClient.readMetadata().setTextgridUri("textgrid:kv2q.0").execute();
    JAXB.marshal(res.getObject(), System.out);
  }

  @Test
  public void Request1000() throws CrudClientException {
    // test if HTTP2 GO-AWAY is handled, (or not existing)
    for (int i=0; i<=1002; i++) {
      var v = crudClient.getVersion().execute();
      if (i > 995) System.out.println(i + " : " + v);
    }
  }

}
