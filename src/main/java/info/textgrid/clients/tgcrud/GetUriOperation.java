package info.textgrid.clients.tgcrud;

import java.util.Arrays;
import java.util.List;

import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;
import jakarta.ws.rs.core.Response;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class GetUriOperation extends CrudOperation {

  private int howMany = 1;

  /**
   * @param tgcrud
   */
  public GetUriOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  /**
   * Create a (list of) TextGrid URI(s)
   *
   * @return the list of TextGrid URIs
   * @throws CrudClientException
   */
  public List<String> execute() throws CrudClientException {

    // Ignore deprecated parameters logID and projectID!
    Response result = getTgcrud().getUriREST(getSid(), "", getHowMany());
    return Arrays.asList(result.readEntity(String.class).split("\n"));

  }

  /**
   * Set a TextGrid SessionID (sid), only needed for reading on private data
   *
   * @param sid
   * @return
   */
  @Override
  public GetUriOperation setSid(String sid) {
    super.setSid(sid);
    return this;
  }

  /**
   * @return
   */
  public int getHowMany() {
    return this.howMany;
  }

  /**
   * @param howMany
   */
  public GetUriOperation setHowMany(int howMany) {
    this.howMany = howMany;
    return this;
  }

}
