package info.textgrid.clients;

import java.net.URI;

import org.eclipse.microprofile.rest.client.RestClientBuilder;

import info.textgrid.clients.tgsearch.FacetQueryBuilder;
import info.textgrid.clients.tgsearch.InfoQuery;
import info.textgrid.clients.tgsearch.NavigationQuery;
import info.textgrid.clients.tgsearch.ProjectQuery;
import info.textgrid.clients.tgsearch.RelationQuery;
import info.textgrid.clients.tgsearch.SearchQueryBuilder;
import info.textgrid.middleware.tgsearch.api.FacetQuery;
import info.textgrid.middleware.tgsearch.api.Info;
import info.textgrid.middleware.tgsearch.api.Navigation;
import info.textgrid.middleware.tgsearch.api.PortalHelper;
import info.textgrid.middleware.tgsearch.api.Relation;
import info.textgrid.middleware.tgsearch.api.Search;

/**
 * Client for TG-Search
 *
 * @see <a href=
 *      "https://textgridlab.org/doc/services/submodules/tg-search/docs/index.html">TG-Search
 *      documentation</a>
 * @author Ubbo Veentjer
 *
 */
public class SearchClient {

  /**
   * URL of the tgsearch endpoint for published data on textgrid production system
   * (textgridlab.org)
   */
  public static final String DEFAULT_PUBLIC_ENDPOINT = "https://textgridlab.org/1.0/tgsearch-public";

  /**
   * URL of the tgsearch endpoint for private data on textgrid production system
   * (textgridlab.org),
   * this is for searching authorized users data
   */
  public static final String DEFAULT_NONPUBLIC_ENDPOINT = "https://textgridlab.org/1.0/tgsearch";

  private Search search;
  private Navigation nav;
  private Info info;
  private Relation relation;
  private FacetQuery facetquery;
  private PortalHelper portalHelper;

  /**
   * Setup the tgsearch client
   *
   * As endpoint the {@link #DEFAULT_PUBLIC_ENDPOINT} and
   * {@link #DEFAULT_NONPUBLIC_ENDPOINT} may be used to work
   * with the textgrid production system textgridlab.org
   *
   * @param endpoint - the http(s) url of tgsearch service
   */
  public SearchClient(String endpoint) {

    this.search = RestClientBuilder.newBuilder()
        // use httpUrlConnection and http1, see https://cwiki.apache.org/confluence/pages/viewpage.action?pageId=49941
        .property("force.urlconnection.http.conduit", true)
        .baseUri(URI.create(endpoint + "/search"))
        .build(Search.class);

    this.nav = RestClientBuilder.newBuilder()
        .property("force.urlconnection.http.conduit", true)
        .baseUri(URI.create(endpoint + "/navigation"))
        .build(Navigation.class);

    this.info = RestClientBuilder.newBuilder()
        .property("force.urlconnection.http.conduit", true)
        .baseUri(URI.create(endpoint + "/info"))
        .build(Info.class);

    this.relation = RestClientBuilder.newBuilder()
        .property("force.urlconnection.http.conduit", true)
        .baseUri(URI.create(endpoint + "/relation"))
        .build(Relation.class);

    this.facetquery = RestClientBuilder.newBuilder()
        .property("force.urlconnection.http.conduit", true)
        .baseUri(URI.create(endpoint + "/facet"))
        .build(FacetQuery.class);

    this.portalHelper = RestClientBuilder.newBuilder()
        .property("force.urlconnection.http.conduit", true)
        .baseUri(URI.create(endpoint + "/portal"))
        .build(PortalHelper.class);
  }

  /**
   * Turn on GZIP compression for communication with service
   *
   * DEPRECATED: please use the config of your implementation e.g.
   * https://lordofthejars.github.io/quarkus-cheat-sheet/#_gzip_support
   *
   * @return
   */
  @Deprecated
  public SearchClient enableGzipCompression() {
    return this;
  }

  /**
   * Prepare a SearchQuery with TG-Search
   *
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-search/docs/api/search.html">Search
   *      Query API</a>
   * @return
   */
  public SearchQueryBuilder searchQuery() {
    return new SearchQueryBuilder(search);
  }

  /**
   * Prepare a FacetQuery with TG-Search
   *
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-search/docs/api/search.html">Search
   *      Query API</a>
   * @return
   */
  public FacetQueryBuilder facetQuery() {
    return new FacetQueryBuilder(facetquery);
  }

  /**
   * Prepare a NavigationQuery with TG-Search
   *
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-search/docs/api/search.html">Search
   *      Query API</a>
   * @return
   */
  public NavigationQuery navigationQuery() {
    return new NavigationQuery(nav);
  }

  /**
   * Prepare a InfoQuery with TG-Search
   *
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-search/docs/api/search.html">Search
   *      Query API</a>
   * @return
   */
  public InfoQuery infoQuery() {
    return new InfoQuery(info);
  }

  /**
   * Prepare a RelationQuery with TG-Search
   *
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-search/docs/api/search.html">Search
   *      Query API</a>
   * @return
   */
  public RelationQuery relationQuery() {
    return new RelationQuery(this, relation);
  }

  public ProjectQuery projectQuery() {
    return new ProjectQuery(portalHelper);
  }

}
