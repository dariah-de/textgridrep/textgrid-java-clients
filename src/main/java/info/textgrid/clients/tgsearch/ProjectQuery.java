package info.textgrid.clients.tgsearch;

import info.textgrid.middleware.tgsearch.api.PortalHelper;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.portal.Project;
import info.textgrid.namespaces.middleware.tgsearch.portal.ProjectsResponse;

/**
 * Search and utility methods related to project specific portal pages
 */
public class ProjectQuery {

  private String sid;
  private PortalHelper portalHelper;
  private boolean searchSandbox = false;

  public ProjectQuery(PortalHelper portalHelper) {
    this.portalHelper = portalHelper;
  }

  public String getSid() {
    return sid;
  }

  /**
   * Set SessionID for nonpublic projects
   *
   * @param sid
   * @return
   */
  public ProjectQuery setSid(String sid) {
    this.sid = sid;
    return this;
  }

  public boolean getSearchSandbox() {
    return searchSandbox;
  }

  /**
   * Wether to include sandbox in search results
   *
   * @param searchSandbox
   * @return
   */
  public ProjectQuery setSearchSandbox(boolean searchSandbox) {
    this.searchSandbox = searchSandbox;
    return this;
  }

  /**
   * List all project names, descriptions and their configurations.
   * These are set by portalconfig.xml files in every project root
   *
   * @return all projectconfigs
   */
  public ProjectsResponse listProjects() {
    return portalHelper.listProjects(sid, searchSandbox);
  }

  /**
   * Show project name, description, configuration and README.md for one
   * specific project. These are set by the portalconfig.xml and the
   * README.md in the project root
   *
   * @param id
   *
   * @return project config for project with given id
   */
  public Project projectDetails(String id) {
    return this.portalHelper.projectDetails(sid, searchSandbox, id);
  }

  /**
   * List metadata of toplevel objects for one specific project. Toplevel objects
   * are editions and collections on the root level of a project
   *
   * @param id    the project id
   * @param start result number to start with
   * @param limit number of entries to return
   *
   * @return metadata of all toplevel objects
   */
  public Response listToplevelAggregations(String id, int start, int limit) {
    return this.portalHelper.listToplevelAggregations(id, sid, start, limit, searchSandbox);
  }
}
