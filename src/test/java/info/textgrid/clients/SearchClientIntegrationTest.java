package info.textgrid.clients;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgsearch.FacetResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;

/**
 * This tests assume that the "Digitale Bibliothek" is available and ingested
 * correctly in elasticsearch
 *
 * @author ubbo
 *
 */
public class SearchClientIntegrationTest {

  static final String TGSEARCH_DEV_PUBLIC_URL = "https://dev.textgridlab.org/1.0/tgsearch-public";
  static final String TGSEARCH_PROD_PUBLIC_URL = "https://textgridlab.org/1.0/tgsearch-public";
  static final String TGSEARCH_LOCAL_URL = "http://localhost:9090";
  static final String DIGIBIB_PROJECTID = "TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c";

  static SearchClient searchClient;

  @BeforeClass
  public static void setUp() throws Exception {
    searchClient = new SearchClient(SearchClient.DEFAULT_PUBLIC_ENDPOINT);
    //searchClient = new SearchClient(TGSEARCH_DEV_PUBLIC_URL);
    //searchClient = new SearchClient(TGSEARCH_PROD_PUBLIC_URL);
  }

  @Test
  public void listProject() {
    TextgridUris res = searchClient.infoQuery().getAllObjects(DIGIBIB_PROJECTID);
    // the whole digital library has more then 20 hits, so the default limit applies
    assertEquals(res.getTextgridUri().size(), 20);
  }

  @Test
  public void list30FromProject() {
    TextgridUris res = searchClient.infoQuery().setLimit(30).getAllObjects(DIGIBIB_PROJECTID);
    // the whole digital library has more then 30 hits, limit applies
    assertEquals(res.getTextgridUri().size(), 30);
  }

  @Test
  public void getChildren() {
    TextgridUris res = searchClient.infoQuery().getChildren("textgrid:kv2p.0");
    // the edition "Caroll, Lewis" contains the uri of "Alice im Wunderland"
    assertTrue(res.getTextgridUri().contains("textgrid:kv2q.0"));
    assertEquals(res.getTextgridUri().size(), 6);
  }

  @Test
  public void getParents() {
    TextgridUris res = searchClient.infoQuery().getParents("textgrid:kv2q.0");
    // "Alice im Wunderland" has the edtiton "Caroll, Lewis" as parent
    assertTrue(res.getTextgridUri().contains("textgrid:kv2p.0"));
    assertEquals(res.getTextgridUri().size(), 3);
  }

  @Test
  public void getditionWorkMeta() {
    Response res = searchClient.infoQuery().getEditionWorkMeta("textgrid:kv2q.0");
    // the result contains the metadata for itself, the parent edition and its work
    assertEquals(res.getResult().size(), 3);
  }

  @Test
  public void testFacetQueryWithTermList() {
    List<String> termList = Arrays.asList("edition.agent.value");
    FacetResponse res = searchClient.facetQuery().setTermList(termList).execute();
    assertEquals(res.getFacetGroup().size(), 1);
    assertEquals(res.getFacetGroup().get(0).getName(), "edition.agent.value");
    assertEquals(res.getFacetGroup().get(0).getFacet().size(), 10);
  }

  @Test
  public void testFacetQueryWithTermListLimit20() {
    List<String> termList = Arrays.asList("edition.agent.value");
    FacetResponse res = searchClient.facetQuery().setTermList(termList).setLimit(20).execute();
    assertEquals(res.getFacetGroup().size(), 1);
    assertEquals(res.getFacetGroup().get(0).getName(), "edition.agent.value");
    assertEquals(res.getFacetGroup().get(0).getFacet().size(), 20);
  }

  @Test
  public void testFacetQueryWithTermListEmptyResults() {
    List<String> termList = Arrays.asList("no-tgmeta-term1", "no-tgmeta-term2");
    FacetResponse res = searchClient.facetQuery().setTermList(termList).setLimit(20).execute();
    assertEquals(res.getFacetGroup().size(), 2);
    assertEquals(res.getFacetGroup().get(0).getFacet().size(), 0);
    assertEquals(res.getFacetGroup().get(1).getFacet().size(), 0);
  }

  @Test
  @Ignore
  // http://textgridlab.org/1.0/tgsearch-public/search?query=isEditionOf:"textgrid:1265s.0"
  public void editionForWorkQuery() {

    Response res = searchClient.searchQuery()
        .setQuery("isEditionOf:\"textgrid:1265s.0\"")
        .addFilter("format:text/tg.edition+tg.aggregation+xml")
        .execute();

    assertTrue(Integer.valueOf(res.getHits()) >= 1);
    String uri = res.getResult().get(0).getObject().getGeneric()
        .getGenerated().getTextgridUri().getValue();
    assertEquals(uri, "textgrid:1265t.0");

  }

  @Test
  // http://textgridlab.org/1.0/tgsearch-public/search?filter=edition.isEditionOf:textgrid:1265s.0
  public void editionForWorkFilter() {

    Response res = searchClient.searchQuery()
        .setQuery("*")
        .addFilter("edition.isEditionOf:textgrid:1265s.0")
        .addFilter("format:text/tg.edition+tg.aggregation+xml")
        .execute();

    assertTrue(Integer.valueOf(res.getHits()) >= 1);
    String uri = res.getResult().get(0).getObject().getGeneric()
        .getGenerated().getTextgridUri().getValue();
    assertEquals(uri, "textgrid:1265t.0");

  }

}
