package info.textgrid.clients.tgcrud;

import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

public class CrudOperation {

  private TGCrudServiceREST tgcrud;
  private String sid = "";
  private String textgridUri;

  public CrudOperation(TGCrudServiceREST tgcrud) {
    this.setTgcrud(tgcrud);
  }

  public String getSid() {
    return this.sid;
  }

  /**
   * Set TextGrid SessionID (sid)
   *
   * @param sid
   * @return
   */
  public CrudOperation setSid(String sid) {
    this.sid = sid;
    return this;
  }

  /**
   * @return
   */
  public TGCrudServiceREST getTgcrud() {
    return this.tgcrud;
  }

  /**
   * @param tgcrud
   */
  public void setTgcrud(TGCrudServiceREST tgcrud) {
    this.tgcrud = tgcrud;
  }

  /**
   * @return
   */
  public String getTextgridUri() {
    return this.textgridUri;
  }

  /**
   * Set TextGrid URI for Object to operate on
   *
   * @param textgridUri
   * @return
   */
  public CrudOperation setTextgridUri(String textgridUri) {
    this.textgridUri = textgridUri;
    return this;
  }

}
