package info.textgrid.clients.tgsearch;

import java.util.ArrayList;
import java.util.List;

import info.textgrid.middleware.tgsearch.api.Search;
import info.textgrid.middleware.tgsearch.api.TGSearchConstants;
import info.textgrid.namespaces.middleware.tgsearch.Response;

/**
 *
 */
public class SearchQueryBuilder {

  private String query;
  private String target;
  private String order;
  private int start = 0;
  private int limit = TGSearchConstants.SEARCH_CLIENT_ITEM_LIMIT;
  private int kwicWidth = TGSearchConstants.KWIC_WIDTH_LIMIT;
  private int wordDistance = -1;
  private boolean resolvePath = false;
  private boolean searchAllProjects = false;
  private boolean searchSandbox = false;
  private List<String> filterList = new ArrayList<String>();
  private List<String> facetList = new ArrayList<String>();
  private int facetLimit = 10;
  private String sid;
  private Search search;

  /**
   * @param search
   */
  public SearchQueryBuilder(Search search) {
    this.search = search;
  }

  /**
   * @return
   */
  public Response execute() {
    return this.search.getQuery(
        getQuery(),
        getTarget(),
        getSid(),
        "", // logstring: deprecated
        "", // sessionid (for tgsearch session): deprecated
        getOrder(), getStart(), getLimit(), getKwicWidth(),
        getWordDistance(), Boolean.toString(getResolvePath()),
        Boolean.toString(getSearchAllProjects()),
        Boolean.toString(getSearchSandbox()), getFilterList(),
        getFacetList(), getFacetLimit());
  }

  /**
   * @param query
   *              the query to set
   * @return
   */
  public SearchQueryBuilder setQuery(String query) {
    this.query = query;
    return this;
  }

  /**
   * @param target
   *               the target to set
   * @return
   */
  public SearchQueryBuilder setTarget(String target) {
    this.target = target;
    return this;
  }

  /**
   * @param order
   *              the order to set
   * @return
   */
  public SearchQueryBuilder setOrder(String order) {
    this.order = order;
    return this;
  }

  /**
   * @param start
   *              the start to set
   * @return
   */
  public SearchQueryBuilder setStart(int start) {
    this.start = start;
    return this;
  }

  /**
   * @param limit
   *              the limit to set
   * @return
   */
  public SearchQueryBuilder setLimit(int limit) {
    this.limit = limit;
    return this;
  }

  /**
   * @param kwicWidth
   *                  the kwicWidth to set
   * @return
   */
  public SearchQueryBuilder setKwicWidth(int kwicWidth) {
    this.kwicWidth = kwicWidth;
    return this;
  }

  /**
   * @param wordDistance
   *                     the wordDistance to set
   * @return
   */
  public SearchQueryBuilder setWordDistance(int wordDistance) {
    this.wordDistance = wordDistance;
    return this;
  }

  /**
   * @param resolvePath
   *                    the resolvePath to set
   * @return
   */
  public SearchQueryBuilder setResolvePath(boolean resolvePath) {
    this.resolvePath = resolvePath;
    return this;
  }

  /**
   * @param searchAllProjects
   *                          the searchAllProjects to set
   * @return
   */
  public SearchQueryBuilder setSearchAllProjects(boolean searchAllProjects) {
    this.searchAllProjects = searchAllProjects;
    return this;
  }

  /**
   * @param searchSandbox
   *                      the searchSandbox to set
   * @return
   */
  public SearchQueryBuilder setSearchSandbox(boolean searchSandbox) {
    this.searchSandbox = searchSandbox;
    return this;
  }

  /**
   * @param filterList
   *                   the filterList to set
   * @return
   */
  public SearchQueryBuilder setFilterList(List<String> filterList) {
    this.filterList = filterList;
    return this;
  }

  /**
   * @param filter
   * @return
   */
  public SearchQueryBuilder addFilter(String filter) {
    this.filterList.add(filter);
    return this;
  }

  /**
   * @param facetList
   *                  the facetList to set
   * @return
   */
  public SearchQueryBuilder setFacetList(List<String> facetList) {
    this.facetList = facetList;
    return this;
  }

  /**
   * @param facet
   * @return
   */
  public SearchQueryBuilder addFacet(String facet) {
    this.facetList.add(facet);
    return this;
  }

  /**
   * @param facetLimit
   *                   the facetLimit to set
   * @return
   */
  public SearchQueryBuilder setFacetLimit(int facetLimit) {
    this.facetLimit = facetLimit;
    return this;
  }

  public SearchQueryBuilder setSid(String sid) {
    this.sid = sid;
    return this;
  }

  public String getQuery() {
    return this.query;
  }

  public String getTarget() {
    return this.target;
  }

  public String getOrder() {
    return this.order;
  }

  public int getStart() {
    return this.start;
  }

  public int getLimit() {
    return this.limit;
  }

  public int getKwicWidth() {
    return this.kwicWidth;
  }

  public int getWordDistance() {
    return this.wordDistance;
  }

  public boolean getResolvePath() {
    return this.resolvePath;
  }

  public boolean getSearchAllProjects() {
    return this.searchAllProjects;
  }

  public boolean getSearchSandbox() {
    return this.searchSandbox;
  }

  public List<String> getFilterList() {
    return this.filterList;
  }

  public List<String> getFacetList() {
    return this.facetList;
  }

  public int getFacetLimit() {
    return this.facetLimit;
  }

  public String getSid() {
    return this.sid;
  }

}
