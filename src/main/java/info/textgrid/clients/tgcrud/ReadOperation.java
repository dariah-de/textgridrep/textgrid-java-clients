package info.textgrid.clients.tgcrud;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import jakarta.ws.rs.core.Response;

import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

public class ReadOperation extends CrudOperation {

  public ReadOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  /**
   * Get the TextGrid Object with TextGridUri
   *
   * @see TextGridObject
   * @return the TextgridObject
   * @throws CrudClientException
   */
  public TextGridObject execute() throws CrudClientException {

    try {
      Response res = getTgcrud().readREST(getSid(), "", new URI(getTextgridUri()));
      return new TextGridObject(null, res.readEntity(InputStream.class));

    } catch (URISyntaxException e) {
      throw new CrudClientException(e);
    }

  }

  /**
   * Set TextGrid URI for Object to operate on
   *
   * @param textgridUri
   * @return
   */
  public ReadOperation setTextgridUri(String textgridUri) {
    super.setTextgridUri(textgridUri);
    return this;
  }

  /**
   * Set a TextGrid SessionID (sid), only needed for reading on private data
   *
   * @param sid
   * @return
   */
  public ReadOperation setSid(String sid) {
    super.setSid(sid);
    return this;
  }

}
