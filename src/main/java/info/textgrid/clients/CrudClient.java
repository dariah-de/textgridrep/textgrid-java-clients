package info.textgrid.clients;

import java.net.URI;

import org.eclipse.microprofile.rest.client.RestClientBuilder;
import info.textgrid.clients.tgcrud.CheckConsistencyOperation;
import info.textgrid.clients.tgcrud.CreateOperation;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.clients.tgcrud.DeleteOperation;
import info.textgrid.clients.tgcrud.GetUriOperation;
import info.textgrid.clients.tgcrud.GetVersionOperation;
import info.textgrid.clients.tgcrud.ReadMetadataOperation;
import info.textgrid.clients.tgcrud.ReadOperation;
import info.textgrid.clients.tgcrud.ReindexMetadataOperation;
import info.textgrid.clients.tgcrud.UpdateOperation;
import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

/**
 * <p>
 * Client for TG-Crud
 * </p>
 *
 * @see <a href=
 *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html">TG-Crud
 *      Documentation</a>
 * @author Ubbo Veentjer
 */
public class CrudClient {

  /**
   * URL of the tgcrud and tgcrud public endpoints of the textgrid production, test, and development
   * systems.
   */
  public static final String DEFAULT_ENDPOINT = "https://textgridlab.org/1.0/tgcrud/rest";
  public static final String DEFAULT_PUBLIC_ENDPOINT =
      "https://textgridlab.org/1.0/tgcrud-public/rest";
  public static final String DEFAULT_DEV_ENDPOINT = "https://dev.textgridlab.org/1.0/tgcrud/rest";
  public static final String DEFAULT_PUBLIC_DEV_ENDPOINT =
      "https://dev.textgridlab.org/1.0/tgcrud-public/rest";
  public static final String DEFAULT_TEST_ENDPOINT = "https://test.textgridlab.org/1.0/tgcrud/rest";
  public static final String DEFAULT_PUBLIC_TEST_ENDPOINT =
      "https://test.textgridlab.org/1.0/tgcrud-public/rest";

  private TGCrudServiceREST tgcrud;

  /**
   * <p>
   * Setup the tgcrud client.
   * </p>
   *
   * As endpoint the {@link #DEFAULT_ENDPOINT} may be used to work with the textgrid production
   * system textgridlab.org
   *
   * @param endpoint - the http(s) url of tgcrud service
   */
  public CrudClient(String endpoint) throws CrudClientException {
    this.tgcrud = RestClientBuilder.newBuilder()
        // use httpUrlConnection and http1, see
        // https://cwiki.apache.org/confluence/pages/viewpage.action?pageId=49941
        .property("force.urlconnection.http.conduit", true)
        .baseUri(URI.create(endpoint))
        .build(TGCrudServiceREST.class);
  }

  /**
   * <p>
   * Turn on GZIP compression for communication with service.
   * </p>
   *
   * DEPRECATED: please use the config of your jax-ws implementation activate gzip
   *
   * @return
   */
  @Deprecated
  public CrudClient enableGzipCompression() {
    return this;
  }

  /**
   * <p>
   * Prepare a Create operation.
   * </p>
   *
   * @see info.textgrid.clients.tgcrud.CreateOperation
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html#create">TG-crud
   *      API - Create</a>
   * @return
   */
  public CreateOperation create() {
    return new CreateOperation(this.tgcrud);
  }

  /**
   * <p>
   * Prepare a Read operation.
   * </p>
   *
   * @see info.textgrid.clients.tgcrud.ReadOperation
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html#read">TG-crud
   *      API - Read</a>
   * @return
   */
  public ReadOperation read() {
    return new ReadOperation(this.tgcrud);
  }

  /**
   * <p>
   * Prepare a ReadMetadata operation.
   * </p>
   *
   * @see info.textgrid.clients.tgcrud.ReadMetadataOperation
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html#readmetadata">TG-crud
   *      API - ReadMetadata</a>
   * @return
   */
  public ReadMetadataOperation readMetadata() {
    return new ReadMetadataOperation(this.tgcrud);
  }

  /**
   * <p>
   * Prepare an Update operation.
   * </p>
   *
   * @see info.textgrid.clients.tgcrud.UpdateOperation
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html#update">TG-crud
   *      API - Update</a>
   * @return
   */
  public UpdateOperation update() {
    return new UpdateOperation(this.tgcrud);
  }

  /**
   * <p>
   * Prepare a Delete operation.
   * </p>
   *
   * @see info.textgrid.clients.tgcrud.DeleteOperation
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html#delete">TG-crud
   *      API - Delete</a>
   * @return
   */
  public DeleteOperation delete() {
    return new DeleteOperation(this.tgcrud);
  }

  /**
   * <p>
   * Prepare a getURI operation.
   * </p>
   *
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html#geturi">TG-crud
   *      - GetURI</a>
   * @return
   */
  public GetUriOperation getUri() {
    return new GetUriOperation(this.tgcrud);
  }

  /**
   * <p>
   * Prepare a getVersion operation.
   * </p>
   *
   * @see <a href=
   *      "https://textgridlab.org/doc/services/submodules/tg-crud/tgcrud-webapp/docs/index.html#getversion">TG-crud
   *      - GetVersion</a>
   * @return
   */
  public GetVersionOperation getVersion() {
    return new GetVersionOperation(this.tgcrud);
  }

  /**
   * <p>
   * Check consistency of a TextGrid object.
   * </p>
   *
   * @return
   */
  public CheckConsistencyOperation checkConsistency() {
    return new CheckConsistencyOperation(this.tgcrud);
  }

  /**
   * <p>
   * Re-index metadata of a TextGrid object.
   * </p>
   *
   * @return
   */
  public ReindexMetadataOperation reindexMetadata() {
    return new ReindexMetadataOperation(this.tgcrud);
  }

  /**
   * <p>
   * Returns the used endpoint.
   * </p>
   *
   * @return
   */
  public String GetTGCrudEndpoint() {
    return this.GetTGCrudEndpoint();
  }

}
