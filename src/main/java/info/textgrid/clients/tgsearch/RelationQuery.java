package info.textgrid.clients.tgsearch;

import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.tgsearch.api.Relation;
import info.textgrid.namespaces.middleware.tgsearch.Response;

public class RelationQuery {

  private Relation relation;
  private String sid;

  public RelationQuery(SearchClient searchClient, Relation relation) {
    this.relation = relation;
  }

  /**
   * Find related objects, return relations as
   * SPO (subject predicate object)
   *
   * @param relation
   *                 the relation to find
   * @param uri
   *                 uri of object
   * @return
   *         list of relations
   */
  public Response getRelated(String relation, String uri) {
    return this.relation.getRelation(sid, "", relation, uri);
  }

  /**
   * Find related objects, return only metadata for related objects,
   * may contain empty nodes if not authorized
   *
   * @param relation
   *                 the relation to find
   * @param uri
   *                 uri of object
   * @return
   *         metadata of relateded objects
   */
  public Response getMeta4Relation(String relation, String uri) {
    return this.relation.getOnlyMetadata(sid, "", relation, uri, "false");
  }

  /**
   * Find related objects, return only metadata for related objects,
   * may contain empty nodes if not authorized
   *
   * @param relation
   *                    the relation to find
   * @param uri
   *                    uri of object
   * @param resolvePath
   *                    if path of object should be retrieved
   * @return
   *         metadata of relateded objects
   */
  public Response getMeta4Relation(String relation, String uri, boolean resolvePath) {
    return this.relation.getOnlyMetadata(sid, "", relation, uri, Boolean.toString(resolvePath));
  }

  /**
   * Find related objects, return metadata for related objects and
   * relations as SPO (subject predicate object)
   * may contain empty nodes if not authorized.
   *
   * @param relation
   *                 the relation to find
   * @param uri
   *                 uri of object
   * @return
   *         metadata and relations
   */
  public Response getRelatedAndMeta(String relation, String uri) {
    return this.relation.getCombined(sid, "", relation, uri);
  }

  public String getSid() {
    return sid;
  }

  public RelationQuery setSid(String sid) {
    this.sid = sid;
    return this;
  }

}
