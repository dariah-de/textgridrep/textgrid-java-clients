package info.textgrid.clients.tgcrud;

import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;

import java.net.URI;
import java.net.URISyntaxException;

import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

public class UpdateOperation extends CrudOperation {

  private TextGridObject textgridObject;

  public UpdateOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  public void execute() throws CrudClientException {

    DataSource dataSource = new InputStreamDataSource(this.textgridObject.getData(), "");
    DataHandler data = new DataHandler(dataSource);

    try {
      getTgcrud().updateREST(getSid(), "", new URI(getTextgridUri()),
          this.textgridObject.getMetadatada(), data);
    } catch (URISyntaxException e) {
      throw new CrudClientException(e);
    }
  }

  public UpdateOperation setTextgridUri(String textgridUri) {
    super.setTextgridUri(textgridUri);
    return this;
  }

  public UpdateOperation setSid(String sid) {
    super.setSid(sid);
    return this;
  }

  public UpdateOperation setObject(TextGridObject textgridObject) {
    this.textgridObject = textgridObject;
    return this;
  }

}
