package info.textgrid.clients.tgcrud;

import java.net.URI;
import java.net.URISyntaxException;
import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;

public class ReindexMetadataOperation extends CrudOperation {

  private String reindexSecret;

  public ReindexMetadataOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  /**
   * Re-index a TextGrid Object
   *
   * @throws CrudClientException
   */
  public void execute() throws CrudClientException {

    try {
      getTgcrud().reindexMetadataREST(this.reindexSecret, new URI(getTextgridUri()));
    } catch (URISyntaxException e) {
      throw new CrudClientException(e);
    }

  }

  /**
   * Set TextGrid URI for Object to operate on
   *
   * @param textgridUri
   * @return
   */
  public ReindexMetadataOperation setTextgridUri(String textgridUri) {
    super.setTextgridUri(textgridUri);
    return this;
  }


  /**
   * Set re-index secret
   *
   * @param reindexSecret
   * @return
   */
  public ReindexMetadataOperation setReindexSecret(String reindexSecret) {
    this.reindexSecret = reindexSecret;
    return this;
  }

}
