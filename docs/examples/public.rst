Working with published data in the TextGridRep
==============================================

For working with published data, no authentication with the TextGrid Repository is required. 
So a sessionID (sid) is not used. 

Simple Query
------------

.. code-block:: java
   :linenos:

    // Create Searchclient for default public endpoint, with GZIP Compression enabled
    SearchClient searchClient = new SearchClient(SearchClient.DEFAULT_PUBLIC_ENDPOINT)
        .enableGzipCompression();
    
    // Query for all documents containing the string alice
    Response response = searchClient.searchQuery().setQuery("alice").execute();
		
    // Dump JAXB-XML content to console
    JAXB.marshal(response, System.out);


Query with filter
-----------------

.. code-block:: java
   :linenos:

    // Create Searchclient for default public endpoint, with GZIP Compression enabled
    SearchClient searchClient = new SearchClient(SearchClient.DEFAULT_PUBLIC_ENDPOINT)
		    .enableGzipCompression();

    // Query for all documents containing the String emilia, and filter for genre:drama
    Response response = searchClient.searchQuery()
		    .setQuery("emilia")
		    .addFilter("work.genre:drama")
		    .execute();

    // print out textgridUris and title
    for(ResultType result : response.getResult()) {
	    String tguri = result.getObject().getGeneric()
			    .getGenerated().getTextgridUri().getValue();
	    String title =result.getObject().getGeneric()
			    .getProvided().getTitle().get(0);
			
	    System.out.println(tguri + ":" + title);
	
    }

In this example the textgrid metadata schema gets important, as the knowledge 
about is is required to locate the place of the title in the JAXB object.


Load Object from TG-Crud
------------------------

.. code-block:: java
   :linenos:

    CrudClient crudclient = new CrudClient(CrudClient.DEFAULT_ENDPOINT)
        .enableGzipCompression();

    TextGridObject tgobj = crudclient.read()
        .setTextgridUri("textgrid:rksq.0").execute();

    // Dump JAXB-XML for metadata to console
    JAXB.marshal(tgobj.getMetadatada(), System.out);

    // print contents of file
    IOUtils.copy(tgobj.getData(), System.out);

