package info.textgrid.clients.tgcrud;

import java.net.URI;
import java.net.URISyntaxException;
import info.textgrid.namespaces.middleware.tgcrud.api.TGCrudServiceREST;
import jakarta.ws.rs.core.Response;

public class CheckConsistencyOperation extends CrudOperation {

  public CheckConsistencyOperation(TGCrudServiceREST tgcrud) {
    super(tgcrud);
  }

  /**
   * Check consistency of a TextGrid object
   *
   * @see TextGridObject
   * @return the TextgridObject
   * @throws CrudClientException
   */
  public Response execute() throws CrudClientException {

    try {
      return getTgcrud().checkConsistencyREST(new URI(getTextgridUri()));

    } catch (URISyntaxException e) {
      throw new CrudClientException(e);
    }

  }

  /**
   * Set TextGrid URI for Object to operate on
   *
   * @param textgridUri
   * @return
   */
  @Override
  public CheckConsistencyOperation setTextgridUri(String textgridUri) {
    super.setTextgridUri(textgridUri);
    return this;
  }

}
