package info.textgrid.clients.tgsearch;

import java.util.ArrayList;
import java.util.List;

import info.textgrid.middleware.tgsearch.api.FacetQuery;
import info.textgrid.namespaces.middleware.tgsearch.FacetResponse;

public class FacetQueryBuilder {

  private List<String> termList = new ArrayList<String>();
  private int limit = 10;
  private String order;
  private String sid;
  private boolean searchSandbox = false;
  private FacetQuery facetquery;

  public FacetQueryBuilder(FacetQuery facetquery) {
    this.facetquery = facetquery;
  }

  public FacetResponse execute() {
    return this.facetquery.facetQuery(
        getTermList(),
        getLimit(),
        getOrder(),
        getSid(),
        getSearchSandbox());
  }

  /**
   * set a list of metadata fields (terms) for which factes should be requested.
   *
   * @param termList metadata fields to get facets for
   * @return
   */
  public FacetQueryBuilder setTermList(List<String> termList) {
    this.termList.addAll(termList);
    return this;
  }

  /**
   * add a metadata field (term) for which factes should be requested.
   *
   * @param term add a metadata field to get facets for
   * @return
   */
  public FacetQueryBuilder addTerm(String term) {
    this.termList.add(term);
    return this;
  }

  /**
   * @param limit the limit to set
   * @return
   */
  public FacetQueryBuilder setLimit(int limit) {
    this.limit = limit;
    return this;
  }

  /**
   * @param order order by count or term ascending (asc) or descending, (desc)
   *              e.g.
   *              setting the string "term:asc". defaults to "count:desc"
   * @return
   */
  public FacetQueryBuilder setOrder(String order) {
    this.order = order;
    return this;
  }

  /**
   * @param searchSandbox the searchSandbox to set
   * @return
   */
  public FacetQueryBuilder setSearchSandbox(boolean searchSandbox) {
    this.searchSandbox = searchSandbox;
    return this;
  }

  public FacetQueryBuilder setSid(String sid) {
    this.sid = sid;
    return this;
  }

  public List<String> getTermList() {
    return termList;
  }

  public int getLimit() {
    return limit;
  }

  public String getOrder() {
    return order;
  }

  public boolean getSearchSandbox() {
    return searchSandbox;
  }

  public String getSid() {
    return sid;
  }

}
