.. tgclients documentation master file, created by
   sphinx-quickstart on Wed May 20 12:51:17 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TextGrid Java Clients
=====================

The TextGrid Java clients provide an easy API for the TextGrid repository services.

Usage
-----

Include the textgrid-clients package in your maven project:

.. code-block:: xml
   :linenos:

   <dependency>
     <groupId>info.textgrid.middleware.clients</groupId>
     <artifactId>textgrid-clients</artifactId>
     <version>3.2.5</version>
   </dependency>

You also need to use the dariah-de nexus repository to retrieve textgrid artefacts

.. code-block:: xml
   :linenos:

   <repositories>
     <repository>
       <id>dariah.nexus.snapshots.http</id>
       <name>DARIAH Nexus Managed Snapshot Repository</name>
       <url>https://ci.de.dariah.eu/nexus/content/groups/public</url>
       <releases>
         <enabled>true</enabled>
       </releases>
       <snapshots>
         <enabled>true</enabled>
       </snapshots>
     </repository>
   </repositories>

Examples
--------

.. toctree::
   :maxdepth: 2

   examples/public.rst


Javadoc
-------

.. toctree::
   :maxdepth: 3

   javadoc/packages.rst


Sources
-------

See tgclients_sources_


Bugtracking
-----------

See tgclients_bugtracking_


License
-------

See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-common/blob/develop/LICENSE.txt
.. _tgclients_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-common/issues?label_name[]=textgrid-clients
.. _tgclients_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-common/-/tree/develop/textgrid-clients
