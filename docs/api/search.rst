searchclient
============

.. java:ref: SearchClient

.. code-block:: java
   :linenos:

   SearchClient searchclient = new SearchClient(TgSearchClient.DEFAULT_PUBLIC_ENDPOINT)
        .enableGzipCompression();
		
   QueryBuilder request = new QueryBuilder()
        .setQuery("pudel")
        .addFilter("work.genre:drama")
        .setLimit(20);

   Response response = searchclient.query(request);
		
   JAXB.marshal(response, System.out);
   System.out.println(response.getHits());
