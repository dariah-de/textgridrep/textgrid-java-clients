package info.textgrid.clients.tgcrud;

import java.io.InputStream;

import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;

/**
 * Representation of a TextGrid Object, consisting of data and metadata
 *
 * @author Ubbo Veentjer
 */
public class TextGridObject {

  private MetadataContainerType metadatada;
  private InputStream data;

  /**
   * Empty Constructor
   */
  public TextGridObject() {}

  /**
   * Constructor which takes metadata object and data
   *
   * @param metadata
   * @param data
   */
  public TextGridObject(MetadataContainerType metadata, InputStream data) {
    this.metadatada = metadata;
    this.data = data;
  }

  /**
   * Constructor which builds an TextGrid metadata object for given format and title with given data
   *
   * @param title the name of the object
   * @param format the format (mime-type) of the object
   * @param data data (content) for textgridobject
   */
  public TextGridObject(String title, String format, InputStream data) {

    ProvidedType providedMeta = new ProvidedType();
    providedMeta.setFormat(format);
    providedMeta.getTitle().add(title);

    GenericType genericMeta = new GenericType();
    genericMeta.setProvided(providedMeta);

    ObjectType objectMeta = new ObjectType();
    objectMeta.setGeneric(genericMeta);

    MetadataContainerType metaContainer = new MetadataContainerType();
    metaContainer.setObject(objectMeta);

    this.metadatada = metaContainer;
    this.data = data;
  }

  /**
   * Get metadata for this object
   *
   * @return
   */
  public MetadataContainerType getMetadatada() {
    return this.metadatada;
  }

  /**
   * Set metadata for this object
   *
   * @param metadatada
   */
  public void setMetadatada(MetadataContainerType metadatada) {
    this.metadatada = metadatada;
  }

  /**
   * Get data of this object
   *
   * @return
   */
  public InputStream getData() {
    return this.data;
  }

  /**
   * Set data (content) of this object
   *
   * @param data
   */
  public void setData(InputStream data) {
    this.data = data;
  }

}
