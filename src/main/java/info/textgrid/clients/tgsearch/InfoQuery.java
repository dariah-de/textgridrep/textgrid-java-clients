package info.textgrid.clients.tgsearch;

import info.textgrid.middleware.tgsearch.api.Info;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;

/**
 *
 */
public class InfoQuery {

  private Info info;
  private String sid;
  private boolean resolvePath;
  private int start = 0;
  private int limit = 20;

  /**
   * @param info
   */
  public InfoQuery(Info info) {
    this.info = info;
  }

  /**
   * List revisions of a textgrid object
   *
   * @param uri
   *            uri of textgrid object
   * @return revisions of object
   */
  public Revisions listRevisions(String uri) {
    return this.info.revisions(uri);
  }

  /**
   * List revisions of a textgrid object, include metadata
   *
   * @param uri
   *            uri of textgrid object
   * @return revisions of object
   */
  public Response listRevisionsAndMeta(String uri) {
    return this.info.revisionsAndMeta(uri, this.sid);
  }

  /**
   * Retrieve metadata for a textgrid object specified by uri
   *
   * @param uri
   *            uri of textgrid object
   * @return metadata for uri
   */
  public Response getMetadata(String uri) {
    return this.info.metadata(uri, this.sid,
        Boolean.toString(this.resolvePath));
  }

  /**
   * Find Path to Edition and Work for given uri.
   *
   * @param uri
   * @return uri
   */
  public PathResponse getPath(String uri) {
    return this.info.getPath(uri);
  }

  /**
   * Get related Metadata for the Object. Also get Metadata for related
   * Edition and Work.
   *
   * @param uri
   * @return uri and sid
   */
  public Response getEditionWorkMeta(String uri) {
    return this.info.getEditionWorkMeta(uri, this.sid);
  }

  /**
   * find parent aggregations for a given uri, searches all the way to the
   * root
   *
   * @param uri
   * @return
   */
  public TextgridUris getParents(String uri) {
    return this.info.getParents(uri);
  }

  /**
   * find children for given aggregation uri, this is a deep search on the
   * whole tree
   *
   * @param uri
   * @return
   */
  public TextgridUris getChildren(String uri) {
    return this.info.getChildren(uri);
  }

  /**
   * find all objects contained in the given project
   *
   * @param projectId
   * @return
   */
  public TextgridUris getAllObjects(String projectId) {
    return this.info.getProjectMembers(projectId, this.start, this.limit);
  }

  /**
   * @return
   */
  public String getSid() {
    return this.sid;
  }

  /**
   * @param sid
   * @return
   */
  public InfoQuery setSid(String sid) {
    this.sid = sid;
    return this;
  }

  /**
   * @return
   */
  public int getStart() {
    return this.start;
  }

  /**
   * @param start
   * @return
   */
  public InfoQuery setStart(int start) {
    this.start = start;
    return this;
  }

  /**
   * @return
   */
  public int getLimit() {
    return this.limit;
  }

  /**
   * @param limit
   * @return
   */
  public InfoQuery setLimit(int limit) {
    this.limit = limit;
    return this;
  }

  /**
   * @return
   */
  public boolean isResolvePath() {
    return this.resolvePath;
  }

  /**
   * @param resolvePath
   * @return
   */
  public InfoQuery setResolvePath(boolean resolvePath) {
    this.resolvePath = resolvePath;
    return this;
  }

}
